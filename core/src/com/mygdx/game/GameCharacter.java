package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;



public class GameCharacter extends Actor
{
    private TextureRegion textureRegion; // has multiple images for animation frames
    private Rectangle rectangle;


    public GameCharacter()
    {
        super();
        textureRegion = new TextureRegion();
        rectangle = new Rectangle();
    }

    public void  setTexture( Texture t)
    {
        textureRegion.setRegion(t);
        setSize(t.getWidth(),t.getHeight());
        rectangle.setSize(t.getWidth(),t.getHeight());
    }

    public Rectangle getRectangle()
    {
        rectangle.setPosition(getX(),getY());
        return rectangle;
    }

    public boolean overlaps( GameCharacter other )
    {
        return this.getRectangle().overlaps(other.getRectangle());
    }

    public void act ( float dt)
    {
        super.act(dt);
    }

    public void draw (Batch batch, float parentAlpha)
    {
        super.draw(batch,parentAlpha);

        Color c = getColor();  // default color is white
        batch.setColor(c.r,c.g,c.b,c.a);
        if ( isVisible() )
        {
            batch.draw(textureRegion,getX(),getY(),getOriginX(),getOriginY(),getWidth(),getHeight(),getScaleX(),getScaleY(),getRotation());
        }

    }

}
