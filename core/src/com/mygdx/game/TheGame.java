package com.mygdx.game;


import com.badlogic.gdx.Gdx;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;


import javax.swing.*;

public class TheGame extends Game
{
    private SpriteBatch batch;



    private Texture winMessageTexture;
    private boolean win;


    private Ducky duck1;
 //   private Pig pig;
    private Bug bug;
    private StickMan stickMan;
    private Stage mainStage;



    // audio stuff
    private Music bgMusic;
    private Sound gunShot;
    boolean gunPlayed = false;

    // text stuffbutton
    private Label scoreLabel;
    private int touchTimes = 0;


    @Override
    public void create()
    {
        batch = new SpriteBatch();
        winMessageTexture = new Texture("win.jpg");


        duck1 = new Ducky();
        duck1.setTexture(new Texture("ducky.png"));
        duck1.setPosition(20,20);


//        pig = new Pig();
//        pig.setTexture(new Texture("pig.png"));
//        pig.setPosition(50,50);


        bug = new Bug();
        bug.setTexture(new Texture("bug.png"));
        bug.setPosition(240,250);

        stickMan = new StickMan(batch);
        stickMan.setPosition(100,100);

        mainStage = new Stage();

        mainStage.addActor(duck1);
   //     mainStage.addActor(pig);
        mainStage.addActor(bug);
        mainStage.addActor(stickMan);

        win = false;

        bgMusic = Gdx.audio.newMusic((Gdx.files.internal("music/ymca.mp3")));
        gunShot = Gdx.audio.newSound((Gdx.files.internal("sounds/gunSound.ogg")));

        float audioVolume = 0.05f;
        bgMusic.setLooping(true);
        bgMusic.setVolume(audioVolume);

        bgMusic.play();

        // text
        Label.LabelStyle labelStyle;
        labelStyle = new Label.LabelStyle();

        labelStyle.font = new BitmapFont(); // to use default font,  Arial 15
        labelStyle.font = new BitmapFont(Gdx.files.internal("cooper.fnt"));
        scoreLabel = new Label("Health:", labelStyle );

        scoreLabel.setText("Score ");
        scoreLabel.setPosition(100,600);
        scoreLabel.setColor(Color.BLUE);
        mainStage.addActor(scoreLabel);

        //////////// Buttons /////////////
        CreateImageButton();  /// <----- Don't forget to do this otherwise your button events won't fire
        // image based buttons
        Gdx.input.setInputProcessor(mainStage);
        // text based buttons, see chapter 6






    }

    public void CreateImageButton()
    {
        Button.ButtonStyle buttonStyle = new Button.ButtonStyle();
        Texture buttonTexture = new Texture(Gdx.files.internal("restart.jpg"));
        TextureRegion buttonRegion = new TextureRegion(buttonTexture);
        buttonStyle.up = new TextureRegionDrawable(buttonRegion);
        Button restartButton = new Button(buttonStyle);
        restartButton.setColor(Color.DARK_GRAY);
        restartButton.setPosition(400,400);



        // add the handler
        restartButton.addListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                Gdx.app.log("Example", "touch started at (" + x + ", " + y + ")");
                scoreLabel.setText("Touch down " + touchTimes);
                touchTimes++;
                return false;
            }


        });

        mainStage.addActor(restartButton);

    }

    public void CreateTextButton()
    {

    }



    public void render()
    {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);


//        if ( pig.overlaps(bug))
//        {
//            win = true;
//        }

        if ( duck1.overlaps(bug))
        {
            win = true;
        }


        mainStage.act(1/30.0f); // checks for movement  and updates position, pass in frames per/sec.   lower it later if logic is slow






        batch.begin();
        if ( !win )
        {


            mainStage.draw();  // draws all actors in the stage in FIFO order

        }
        else
        {
            if ( !gunPlayed )
            {
                // only play gun sound once
                gunShot.play(1.0f);
                gunPlayed = true;

            }

            batch.draw(winMessageTexture,180,180);

        }
        batch.end();

//        batch.begin();
//        batch.draw(currentFrame, origin_x, origin_y);
//        batch.end();


    }

}
