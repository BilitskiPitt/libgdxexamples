package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.utils.Array;

// could be immproved by making animated game character clas
public class StickMan extends Actor
{
    // animated object
    private static float FRAME_DURATION = .05f;
    private TextureAtlas charset;
    private TextureRegion currentFrame;
    private Animation runningAnimation;
    private float elapsed_time = 0f;
    Array<TextureAtlas.AtlasRegion> runningFrames;

    private SpriteBatch batch;
    private boolean keyPressed = false;

    public StickMan(SpriteBatch _batch)
    {
        super();

        batch = _batch;
        // animation
        charset = new TextureAtlas( Gdx.files.internal("spritesheets/charset.atlas") );
        runningFrames = charset.findRegions("running");
        runningAnimation = new Animation(FRAME_DURATION, runningFrames, Animation.PlayMode.LOOP);
        TextureRegion firstTexture = runningFrames.first();
        currentFrame = (TextureRegion) runningAnimation.getKeyFrame(0  );

        setPosition(100,100);
        setWidth(firstTexture.getRegionWidth());
        setHeight(firstTexture.getRegionHeight());


        // change the origin to be the CENTER of the image rather than bottom left corner
        // the rotation will rotate about its center instead
        setOriginX(firstTexture.getRegionWidth()/2.0f);
        setOriginY(firstTexture.getRegionHeight()/2.0f);



        // just showing that we "can" add actions
        Action spin1 = Actions.rotateBy(360,5);
        Action delay1 = Actions.delay(5.0f); // in seconds
        Action spin2 = Actions.rotateBy(360,5);
        Action delay2 = Actions.delay(5.0f); // in seconds
        Action grow = Actions.scaleTo(10,10,1);
        Action shrink = Actions.scaleTo(1,1,1);
        SequenceAction goCrazy = new SequenceAction();
        goCrazy.addAction(spin1);
        goCrazy.addAction(delay1);
        goCrazy.addAction(spin2); // spin1 is consumed
        goCrazy.addAction(delay2);
        goCrazy.addAction(grow);
        goCrazy.addAction(shrink);
        this.addAction(goCrazy);
    }

    public void act (float dt)
    {
        super.act(dt);


        elapsed_time += Gdx.graphics.getDeltaTime();


        keyPressed = false;
        if ( Gdx.input.isKeyPressed(Input.Keys.LEFT))
        {
            this.moveBy(-1,0);
            keyPressed = true;
        }
        if ( Gdx.input.isKeyPressed(Input.Keys.RIGHT))
        {
            this.moveBy(1,0);
            keyPressed = true;
        }
        if ( Gdx.input.isKeyPressed(Input.Keys.UP))
        {
            this.moveBy(0,1);
            keyPressed = true;
        }
        if ( Gdx.input.isKeyPressed(Input.Keys.DOWN))
        {
            this.moveBy(0,-1);
            keyPressed = true;
        }

        // only animate when user presses a key and it is moving
        if ( keyPressed )
        {
            currentFrame = (TextureRegion) runningAnimation.getKeyFrame(elapsed_time);
        }
        else
        {
            // here we would select the frame of the person standing still if it existed.
            // for now frame 10 looks like the closest thing to someone standing still with both feet on the ground
            currentFrame = runningFrames.get(10);
        }

    }



    public void draw(Batch batch, float parentAlpha)
    {
        super.draw(batch, parentAlpha);
        batch.draw(currentFrame, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
    }
}
